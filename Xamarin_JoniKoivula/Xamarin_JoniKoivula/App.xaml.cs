﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Xamarin_JoniKoivula
{
    public partial class App : Application
    {
        public static IList<string> Conversions { get; set; }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
            MainPage.Title = "Joni Koivula";

            Conversions = new List<string>();

            /*
            Conversions.Add("Example 1 Conversion History \n " + DateTime.Now.ToString());
            Conversions.Add("Example 2 Conversion History \n " + DateTime.Now.ToString());
            */
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
