﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Xamarin_JoniKoivula
{
    public partial class MainPage : ContentPage
    {
        //string targetCurrency;
        public MainPage()
        {
            InitializeComponent();
        }

        async void historyPage_btn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HistoryPage());
        }

        private void convertCurrency_btn_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(entryCurrency.Text))
            {
                double convertedCurrency = 0;
                string conversionResultText = "";

                string targetCurrency = targetCurrencyPicker.Items[targetCurrencyPicker.SelectedIndex];
                

                if (targetCurrency == "USD")
                {
                    convertedCurrency = double.Parse(entryCurrency.Text) * 1.19;
                    conversionResultText = entryCurrency.Text + " euros = " + convertedCurrency + " dollars";
                }
                else if (targetCurrency == "GBP")
                {
                    convertedCurrency = double.Parse(entryCurrency.Text) * 0.91;
                    conversionResultText = entryCurrency.Text + " euros = " + convertedCurrency + " pounds";
                }
                else if (targetCurrency == "RUB")
                {
                    convertedCurrency = double.Parse(entryCurrency.Text) * 68.5;
                    conversionResultText = entryCurrency.Text + " euros = " + convertedCurrency + " ruble";
                } else { conversionResultText = "Conversion wasnt successful"; }

                //Lisätään valuutan muutos App tiedostossa olevaan listaan.
                //En saanut DateTime-aikaa rivittymään toiselle riville, joten se menee vähän piiloon HistoryPage näkymässä
                App.Conversions.Add(conversionResultText + " / " + DateTime.Now.ToString());

                convertedCurrency_lbl.Text = conversionResultText;
            }
            
        }
    }
}
